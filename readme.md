# Data Science timetabling analysis project
Karim El Assal (s1097539), January 2017

To prepare the data:

1. Install a MySQL server and create two database: tt_ut and tt_sax.
2. Create the database structures with the respective SQL files in the `/data preparation/*/` directories.
3. Download the original data files from Blackboard.
4. Save `UT_courses_Osiris_with_teacher_2013-2014.xlsx` and `UT_courses_Osiris_with_teacher_2014-2015.xlsx` in CSV format with tabs as delimeters and no enclosure character. Rename the files to `Docentenoverzicht_YEAR_Osiris.csv`, where `YEAR` is the starting calendar year of the academic year. Remove the first few useless rows (and possibly also the last).
5. For Saxion: open the XLSX activity file in Excel. Replace all double quotes with single quotes to avoid problems with Kettle. Save the XLSX activity file in CSV format and use `;` as the delimeter and `"` as enclosure character.
6. Use Pentaho Data Integration Spoon/Kettle with the files `sax2db.ktr`, `utactivities2db.ktr` and `utdocenten2db.ktr` to import the data in the MySQL database.
7. Make sure the DBMS connection keep-alive interval and DBMS connection read time out values are increased to at least 10 hours. In MySQL Workbench, these settings can be found under `Edit > Preferences... > SQL Editor > MySQL Session`.
8. For both databases, execute the normalize.sql file in the `/data preparation/*/` directories.

Execute any analysis queries from the `/KPIs/` and `/occupancy/` directories.