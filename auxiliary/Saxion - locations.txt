
City        Location        wing                room prefix

Enschede    Ko Wieringa     Elderink            E
                            Forum               F
                            Haanstra            H
                            Schierbeek          S
                            Wolvecamp           W
            Epy Drost                           G
            Edith Stein     Ainsworth           N
                            Stork               O
                            Hofstede-Crull      P
                            Hazemeijer          Q
            ITC                                 I
            Randstad                            R, RA, RB, RC
Deventer    Handelskade                         A, B, C, D
            SVB                                 X
            ROC                                 ROC
            ROC gymzaal                         ROC gymzaal
Apeldoorn   Spoorstraat                         K, KC