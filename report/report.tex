%%%%%%%%%%%%%%%%%%%%%%%%%%% asme2ej.tex %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Template for producing ASME-format journal articles using LaTeX    %
% Written by   Harry H. Cheng, Professor and Director                %
%              Integration Engineering Laboratory                    %
%              Department of Mechanical and Aeronautical Engineering %
%              University of California                              %
%              Davis, CA 95616                                       %
%              Tel: (530) 752-5020 (office)                          %
%                   (530) 752-1028 (lab)                             %
%              Fax: (530) 752-4158                                   %
%              Email: hhcheng@ucdavis.edu                            %
%              WWW:   http://iel.ucdavis.edu/people/cheng.html       %
%              May 7, 1994                                           %
% Modified: February 16, 2001 by Harry H. Cheng                      %
% Modified: January  01, 2003 by Geoffrey R. Shiflett                %
% Use at your own risk, send complaints to /dev/null                 %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%% use twocolumn and 10pt options with the asme2ej format
\documentclass[twocolumn,10pt]{asme2ej}

\usepackage{epsfig}             % for loading postscript figures
\usepackage[inline]{enumitem}   % inline enumeration
\usepackage{xcolor}             % inline enumeration symbol coloring
\usepackage[hyphens]{url}       % for correct URL breaking in references
\usepackage{hyperref}           % for URL
\usepackage[noabbrev]{cleveref} % for clever referencing labels

%% The class has several options
%  onecolumn/twocolumn - format for one or two columns per page
%  10pt/11pt/12pt - use 10, 11, or 12 point font
%  oneside/twoside - format for oneside/twosided printing
%  final/draft - format for final/draft copy
%  cleanfoot - take out copyright info in footer leave page number
%  cleanhead - take out the conference banner on the title page
%  titlepage/notitlepage - put in titlepage or leave out titlepage
%  
%% The default is oneside, onecolumn, 10pt, final


\title{Schedule analysis of two educational institutions to support the data-driven decision process}

%%% first author
\author{Karim M. El Assal
    \affiliation{
	Master student\\
	Department of Computer Science\\
	University of Twente\\
	Drienerlolaan 5, 7522 NB Enschede\\
    k.m.elassal@student.utwente.nl\\
    \\
    Source file repository:\\ \url{https://bitbucket.org/iisys/data-science-analysis.git}
    }	
}



\begin{document}

\maketitle    

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{abstract}
{\it 
The results of the National Student Enquiry represent the opinion of Dutch students about their educational institutions. The University of Twente and Saxion Hogeschool want to improve their students' opinion by improving their scheduling process. A first step is to examine whether both institutions actually comply to their own key performance indicators, and if not, on which they should focus their effort to improve on. This paper shows the results of an analysis of those key performance indicators, accompanied with a few recommendations per institute. Only two of the 26 key performance indicators are complied to. We recommend both institutions primarily focus on improving the minimum amount of hours per day for students, as our results indicate that non-compliance to this KPI has the most impact.
}
\end{abstract}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Introduction}

% intro part 1: context
Human resource scheduling is and has always been an issue in large organizations Problems especially arise when the amount of constraints to take into account grows large. An employee's available hours, preferable hours, geographical availability, geographical preference and a co-worker's availability are examples of relevant considerations. Constraints are also abundantly present in the case of educational institutions and their enforcement is interesting to review. Methods such as greedy heuristics, a Lagrangian relaxation algorithm~\cite{Carter2001}, constraint logic programming~\cite{Goltz98constraint-basedtimetabling},  integration of management science and artificial intelligence techniques~\cite{glover1986general}, integer programming~\cite{mohan2008scheduling} and column generation~\cite{al2008column} are developed to succesfully manage the scheduling process.

% intro part 2: need
Organisations always strive to optimalize their scheduling. Overall employee and peer satisfaction depends largely on, for example, the (perceived) amount of wasted time, the total work hours and the length of a working day. Specifically for educational institutions the student's opinion about their educational program's scheduling is important. In the Netherlands, this opinion is reflected in the National Student Enquiry\footnote{in Dutch: Nationale Studenten Enqu\^{e}te} (NSE). A very large percentage of to-be students base their choice of educational institution on the results of the NSE. It's clear these results are important. The Saxion Hogeschool and the University of Twente (UT) both scored relatively bad in 2016~\cite{NSE2016}. One effort to improve this score, is to improve the timetabling process. A first step, however, is to check if there is a fundamental problem with the scheduling by analyzing whether these two institutions actually follow their own key performance indicators (\textit{KPIs}). Goltz et al. called these \textit{hard constraints}~\cite{Goltz98constraint-basedtimetabling}.

% intro part 3: task
To assess whether the institutions comply to their own KPIs, we have examined and analyzed the data they made available of the past few years. This paper shows which methods are used and what the results are. The conclusion contains a recommendation as to which part of the scheduling each institution might prioritize upon. Furthermore, we had the intention to also analyze and report on the occupation percentages of rooms and buildings over time, so both institutions can get a feeling of how much space they waste and if anything about that has changed over the years. However, due to time limitations, this fell outside of the scope of this research project.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Background}
% the course and its goal
This research project was part of the master course Data Science (course code 201400174) at the University of Twente. The goal of the course is to learn to do a data analysis research project, after getting familiar with relevant technologies and techniques during the course's practical assignments. These practical assignments are completely separate from the research project, although they can (and should) be chosen according to their relevance to the project.

% the project and its goal
The timetabling project has been commissioned by the scheduling department of the University of Twente and the Hogeschool Saxion and the project owner is \href{r.a.oudevrielink@utwente.nl}{R.A. Oude Vrielink}. The goal of the project is to improve scheduling at the two institutions, based on factual, historical data. The results and recommendations are thus very important parts for the stakeholders.

% your chosen topics.
The topics we chose during the practical assignment phase of this course were not directly relevant to our research project. This is an exception to the rule and has to do with the fact that we finished the practical assignments one year earlier in a previous edition of this course. Our topics then were XML databases and the semantic web. \Cref{sec:methodology} explains why the techniques discussed in those topics weren't used during this research project. Even though our original topics weren't directly relevant, we did look into the data preparation and visualization topic because it does have immediate relevance to this project.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Methodology}\label{sec:methodology}
Our used methods fall into two categories: our workflow and the format of our results, called our \textit{KPI metrics}.
% workflow/dataflow
\subsection{Workflow}\label{subsec:workflow}
Our chosen workflow was probably not the most optimal one, but it was chosen so the setup phase was as short as possible. We were pressed for time a bit, so we did not had the luxury of having sufficient time to get familiar with the best technologies. Due to that reason, we chose the data(/work) flow as shown in \cref{fig:dataflow}. The data was provided to us in CSV format. Using Pentaho Kettle/Spoon\cite{PentahoKettle}, this data was imported in step (1) into a MySQL database with only basic trimming of the data. In step (2), the still-raw data was copied and formatted into a star schema using SQL queries in the MySQL Workbench. The SQL queries that queried for results that could be analyzed were performed in step (3), also in the MySQL Workbench. The results were saved in CSV format and transformed into XLSX format using Microsoft Excel in step (4). Excel was then used to calculate the result metrics, which will be described in \cref{subsec:kpimetrics}. The reason basic SQL and Excel were used, was that we have experience with these technologies and thus did not cost any time for us to set up. It was a way of getting results fast without spending time on getting familiar with new tools.

\begin{figure}[h]
    \centering
    \includegraphics[width=0.4\textwidth]{diagrams/dataflow}
    \caption{The flow of data during our research}
    \label{fig:dataflow}
\end{figure}

%  normalization
%   2fn & next class/teacher
%   not pressed for storage (but a bit for time, so: Excel)
Step (1) essentially involved copying the data into a relational database. Step (2) involved normalizing the data into the second normal form\cite{codd1971normalized} and adding some derived data. The second normal form was sufficient for this project, because we don't have relevant storage restrictions and this enables simple but useful SQL queries. \Cref{fig:Saxion_starscheme,fig:UT_starscheme} show the (simplified) resulting star scheme of step (2). A few notes on the choices and derived values of these schemas:
\begin{description}
    \item[Dates] Data about the individual dates was extracted from the Saxion data and derived from the UT data. This was necessary for grouping on weeks and quartiles. With the UT data, this proved to be a non-trivial task. For instance, boundaries of the quartiles were entered manually and it was hard to account for holidays. Also, because all analyses work with the date and/or times of the activities, records without date or times were purged.
    \item[Duration] Each activity's duration was calculated and added in the table to support efficient calculations with it. With the UT data, the duration value did \textit{not} include the breaks between the official lecture hours. If, for example, an activity started at 8:45 and ended at 10:30, its duration was 1:30 hours because the break between 9:30 and 9:45 was not included.
    \item[Class type] The class type is represented in the \texttt{timetype} field of the Saxion's \texttt{class} data. It holds a value of V or D, respectively representing a class in full-time or part-time education. This distinction is necessary, because some KPI specifications are slightly different for both types of education.
    \item[Next] This field is one of the most interesting of the derived data and was essential for analyzing free hours. For the Saxion data, this is actually the \texttt{next\_class\_activity} and the \texttt{next\_teacher\_activity} fields. For UT data, it is only the \texttt{next\_class\_activity} field. These fields represent the time until the next class, teacher or study programme activity, respectively. For the UT data, this value only takes into account educational activities, so any activities with the types \texttt{BOEK}, \texttt{ONO} or \texttt{ZZ*} are ignored. Adding this field made the query runtime increase significantly, due to the fact that this adds a \texttt{SELECT} query to every insert. For Saxion data, this increased the normalization script runtime to about 7 hours, while for UT data this runtime increased to about 3.5 hours. This required an increase of the DBMS connection settings to account for the increased query runtime.
\end{description}
This normalization step counted numerous iterations due to new insight gained and new data or data formats needed during step (3).

\begin{figure}[h]
    \centering
    \includegraphics[width=0.4\textwidth]{diagrams/Saxion_datascheme}
    \caption{Simplified, normalized star data scheme of the Saxion data}
    \label{fig:Saxion_starscheme}
\end{figure}
\begin{figure}[h]
    \centering
    \includegraphics[width=0.4\textwidth]{diagrams/UT_datascheme}
    \caption{Simplified, normalized star data scheme of the UT data}
    \label{fig:UT_starscheme}
\end{figure}


%  KPI metrics
\subsection{KPI metrics}\label{subsec:kpimetrics}
The total amount of KPIs to analyze are 9 and 17 for the UT and Saxion, respectively. We needed a way of comparing the KPIs per institution, so we could give an indication of which KPI has the most impact. We came up with four metrics that can be relevant, which are described in the following list. To illustrate this metrics, we'll use the Saxion KPI that dictates that all students must have a minimum of four lesson hours per day. We'll call it our example, but it is an actual result of our research. The corresponding result graph is shown in \cref{fig:kpimetrics_example}.
\begin{figure}[h]
    \centering
    \includegraphics[width=0.5\textwidth]{diagrams/kpimetrics_example}
    \caption{The graph of interesting days for the Saxion KPI that dictates each class should have at least four lesson hours per day}
    \label{fig:kpimetrics_example}
\end{figure}
\begin{description}
    \item[Interesting days] This metrics doesn't say that much on its own, but is used in the next metric. Each KPI has a result set that covers a certain amount of relevant points in time. In our example, that amount is about 232,000 days. The interesting days are the days that a class does \textit{not} adhere to the KPI. In our example, this value is 20.0\%.
    \item[Interesting days per class/teacher/programme on average] The interesting days are grouped per class/teacher/programme (\textit{CTP}), so each CTP has a percentage of interesting days. This metric is the average of that percentage and it indicates the magnitude of the affected CTP. In our example, this value is 27.1\%, thus saying that (on average) 27.1\% of a CTP's analyzed days are not adhering to the KPI.
    \item[Correlation] This is the correlation between the total amount of analyzed days and the amount of interesting days, per CTP. This correlation \textit{might} indicate that implementation of the corresponding KPI has a structural problem and that it does not just concern incidental errors. Note the emphasis on \textit{might}. In our examle, the correlation is 0.64. This is as high as it gets for the Saxion KPIs and might indicate that the violation of this KPI is a structural one.
    \item[Full (C/T/P) Equivalent] This is a metric that indicates the amount of the affected CTP. It is the sum of the interesting day percentages of all CTPs. In our example, the value is a large 601.3. The unit of this value is either classes, teachers or programmes, but has been omitted to emphasize that this is not an actual amount of classes, teachers or programmes.
\end{description}
Note that these metrics are \textit{not} proven, absolute metrics, but are only meant to indicate relative significance.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Results}\label{sec:results}
% overview graphs
The KPIs themselves are listed in \cref{app:kpis}. \Cref{fig:results} shows the analysis results of all KPIs of both institutions. The one exception is the UT KPI that dictates the minimum occupancy per room. The results of that KPI are shown separately in \cref{fig:results_ut_occupancy} because they do not directly affect a class, teacher or programme and thus require a different result format.

These are the results of the analysis of the complete data set, so from the academic years 2013-2014, 2014-2015, 2015-2016 and a small part of 2016-2017, unless otherwise indicated.

\begin{figure*}[h]
    \centering
    \includegraphics[width=1.1\textwidth]{diagrams/results}
    \caption{The KPI metrics resulting from our analysis. The correlation metric is in the KPI's label. No correlation metric means that the KPI was not analyzed. Remember that the FCE/FTE/FPE indicates the amount of CTPs affected, the interesting days on average indicates the magnitude of the CTPs affected and a high correlation indicates that not complying to the KPI is a structural problem.}
    \label{fig:results}
\end{figure*}
\begin{figure}[h]
    \centering
    \includegraphics[width=0.5\textwidth]{diagrams/results_ut_occupancy}
    \caption{Occupancy of rooms at the UT during academic years 2013-2014, 2014-2015 and 2015-2016}
    \label{fig:results_ut_occupancy}
\end{figure}

As one might notice, two KPIs don't have metrics. We did not analyze the Saxion KPIs \textit{teachers: day length} and \textit{teachers: maximum lesson hours}. The first one was skipped because it is very similar to the Saxion student KPI equivalent, with one big difference: there's no real part-time/full-time distinction between teachers. Between 60 and 70\% of all teachers teach both part-time and full-time classes, making any analysis of this KPI almost useless. The second KPI was skipped because it was very hard to determine which hours classify as lesson hours, let alone \textit{plenary lectures}. The final reason to skip both KPIs was that they are both very similar to the Saxion KPI \textit{teachers: clock hours}. Instead, we look to the last mentioned KPI.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Discussion}\label{sec:discussion}
% notes to the data for each KPI
%   note that it was discussed after the presentation that the data might not have been complete (said the data owners)
These are the points of discussion relevant to the KPIs. They either increase or decrease the accuracy of the results, which is indicated with each discussion point. The \textbf{D*} tags correspond to the tags used with the KPIs in \cref{fig:results} and should be read side by side.

\begin{description}
    \item[D1 (Saxion): removed irrelevant activities]
    Activities that are irrelevant were recognized by searching their \texttt{name} and \texttt{remarks} fields for the values \texttt{Toets}, \texttt{exam}, \texttt{PlusProgramma}, \texttt{project}, \texttt{stage}, \texttt{studieloopbaan}, \texttt{SLB}, \texttt{workshop}, \texttt{extern}, \texttt{overleg}, \texttt{vergadering}, \texttt{honours}, \texttt{voorlichting} and \texttt{activiteit}. If any of these strings were found, the activities were not taken into account. This should increase the accuracy of the results.
    
    \item[D2 (Saxion): full-/part-time determination]
    Determining whether a class is full-time or part-time is not completely accurate. With most classes, this data can be extracted from the class code. A large amount of class codes, however, do not respect the class code standards of {\tiny \texttt{\{studyprogramme\}\{year\}\{full-/parttime\}\{abc\}}}. This has implications on the accuracy of results for KPIs that deal with class-type-dependent requirements. The Saxion KPI \textit{students: day length}, for example, dictates that a day for part-time classes can last longer than for a full-time class. If a class is erroneously classified as full-time, it thus ends up (wrongfully) in the \textit{interesting days} for that KPI. This inaccuracy of the full-time/part-time determination further decreases the accuracy of the results.
    
    \item[D3 (Saxion): no first week]
    The first week of the academic year seemed to be irregularly filled with activities. To increase the accuracy of the results, we removed this first week from the analyzed data.
    
    \item[D4 (Saxion): contact activities]
    The only way to determine which activities are contact activities is by checking if there is a teacher assigned to the activities. Activities for these KPIs have no teacher assigned. However, we could not check if this is an accurate way of recognizing contact activities, so we do not know the impact on the overall accuracy of the result.
    
    \item[D5 (Saxion): no 15 minute breaks]
    Any possible 15 minute breaks between lesson hours are not taken into account while analyzing free hours. This should increase the accuracy of the results.
    
    \item[D6 (Saxion): no intern activities] 
    All internship-related activities are removed from this data set.
    
    \item[D7 (Saxion): condensed hours]
    We have not been able to get completely reliable results. This is due to the nature of our data setup: per activity, we know the time until the next class or teacher activity. However, when grouping activities of the same day and aggregating data from that group, the order is lost. Summing up the durations of all activities per day is easy, but recognizing whether the one activity that is followed by sufficient break time is in that group of activities, is not possible. Luckily, about 80\% and 86\% (for students and teachers, respectively) of all scheduled days in these result sets have only one or two activities, enabling us to give reliable results. In conclusion: the results of these data sets are only about the 80\% or 86\% we know we can give reliable results about. This decreases the overall accuracy of the KPI's result, but increases its reliability a bit.
    
    \item[D8 (Saxion): only lessons]
    Only lesson-type activities are taken into account, so as to exclude incidental activities. This is probably not sufficient to filter out all irrelevant activities, but it does increase the accuracy of these results a bit.
    
    \item[D9 (Saxion): building changes]
    It is nearly impossible to determine the amount of building changes accurately, because a class can have lessons in multiple buildings at once (meaning the class is split up). We can however look at the amount of different buildings a class had lessons in during the day. That is what the results actually are: whether a class had lessons in three different buildings, not if a class changed buildings too often. This decreases the accuracy of the results and the reality is probably that far less cases comply to the KPI.
    
    \item[D10 (Saxion): morning time]
    The end of a morning was set to 10:00.
    
    \item[D11 (Saxion): compliance!]
    As one might have noticed: the results of these KPI analyses are positive. According to the data, Saxion completely complies with these KPIs. There is some inaccuracy involved, but it seems that the results are at least very promising.
    
    \item[D12 (UT): student identification]
    We've had some trouble relating different activities to each other that belong to the same students. This is necessary for e.g. counting the amount of contact hours per day. We eventually simply used the \texttt{hostkey2} field of the original data, since that's supposed to contain a reference to a student's study programme. This isn't completely accurate in reality, which decreases the accuracy of results that are based on this value.
    
    \item[D13 (UT): break time]
    When calculating the duration of activities, all time slots that are not part of the official lecture hours are excluded. This means the 15 minute breaks between lecture hours are excluded, even when an activity starts before and ends after that break. An example: if an activity lasts from 8:45 to 10:30, its duration is 1:30 hours because the break between 9:30 and 9:45 is ignored. This is also true for the lunch break that lasts from 12:30 to 13:45. This increases the accuracy of the results a bit, and certainly levels the playing field between KPIs.
    
    \item[D14 (UT): contact activities]
    These KPIs are only about contact hours. To that end, we've only included activities of the types that indicate contact with a teacher: \texttt{ASM}, \texttt{COL}, \texttt{CQ}, \texttt{DTOETS}, \texttt{TOETS}, \texttt{EXC}, \texttt{HC}, \texttt{OVO}, \texttt{PJB}, \texttt{PRA}, \texttt{PRS}, \texttt{RES}, \texttt{RESP}, \texttt{WC}, \texttt{WG} and \texttt{ZMB}. Types such as \texttt{PJO} (project unsupervised) are ignored. This should increase the accuracy of the results, as the relevant KPIs are specifically about contact hours.
    
    \item[D15 (UT): maximum hours]
    The original KPI mentions the maximum of 11 college hours being 8:15 clock hours between the start of the first lecture and the end of the last lecture. This definition is inaccurate, as a full day from 8:45 to 17:30 already is 8:45 clock hours. As an alternative but highly similar interpretation, we've analyzed the sum of all activity durations (excluding breaks, of course). This increases the accuracy of the results.
    
    \item[D16 (UT): late nights]
    We've assumed a 'late night' to be a day having an activity in the last two lecture hour slots of the day: after 20:45. This should increase the accuracy of the results.
    
    \item[D17 (UT): friday evenings]
    We've assumed a friday evening starts at 17:45. This is because some activities end at 17:45 and we didn't want those to be categorized as friday evening activities. This increases the accuracy of the results.
    
    \item[D18 (UT): teacher activites]
    This involves a fundamental problem with the originally provided data format. The teachers are linked to the activities via the course codes. This means that teachers are linked to \textit{all} activities of their linked courses. The accuracy of results are thus inaccurate, and we even go as far to say the results are highly inaccurate.
    
    \item[UT occupancy] 
    There are several points of discussion with the analysis of the UT's room occupancy. \begin{enumerate*}[label={\alph*)},font={\color{red!50!black}\bfseries}]\item We removed the buildings \textit{Vrijhof}, \textit{Gallery}, \textit{Sportcentre}, \textit{Therm} and \textit{extern}. They have irregular occupancy and do not need to be taken into account for this analysis. \item Only academic years 2013-2014, 2014-2015 and 2015-2016 are taken into account because a combination of point \texttt{d} in this list and the fact that there is data available of those complete years. \item The occupancy per room is calculated as follows: the total amount of occupied time divided by the amount of time it can be occupied. Again: breaks are not taken into account. The amount of time it can be occupied is eight college hours per day, five days per week, eight regular education weeks per quartile (disregarding two exam weeks) and four quartiles per academic year. \item Only the first eleven weeks per quartile are counted. This is to account for activities scheduled during the holidays. \end{enumerate*}\\
    All reasons just mention are meant to increase the accuracy of the result. In reality, the results are probably worse.
\end{description}

On top of all these points of discussion, it should be noted that the process of extracting the data from its source might contain errors. The results of this project were presented on January 31st, 2017, in the presence of the UT employees responsible for the timetabling process and providing the data. They argued, after discussing results of another presentation, that there might be errors in the extraction process. One of those possible errors is that recent sequences of activities were extracted as one activity. One of the consequences this might have for our results is that the UT room occupancy is higher in reality.



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Conclusions and recommendations}\label{sec:conclusion}
% short overview
In this research project we've examined the available timetabling data of the University of Twente and Saxion Hogeschool. We've analyzed how both institutions comply to their own key performance indicators and indicate how large the impact of non-compliance is on their students and teachers.

% recommendations
Based on the results of the analysis, we recommend Saxion to primarily focus on the KPI \textit{students: minimum hours}. It has the highest FCE value of 601.3 and the highest interesting days on average, being 27.13 days. These two relatively high values indicate that both the amount of classes affected and the magnitude of the effect are relatively large. On top of that, the correlation between the scheduled days and the days of non-compliance is 0.64, indicating that non-compliance to the KPI is structural.
In addition to the \textit{students: minimum hours} KPI, we recommend prioritizing the \textit{students: maximum hours} KPI next. It has the second-highest FCE value with 183.1 and with a value of 0.60 also has a pretty high correlation. The amount of interesting days on average is in third place with a value of 8.4 days. The reason we recommend prioritizing this KPI and not the \textit{students: dinner time} KPI (which has 22.9 interesting days on average), is that we believe the \textit{students: maximum hours} KPI has the most impact on the NSE score. We believe students are more dissatisfied with too long days than with the inconvenience of having dinner on irregular times\footnote{Note that this is \textit{not} based on any scientific fact, but solely on our own experience}.

For the University of Twente, we also recommend to primarily focus on the KPI with the highest values: \textit{students: minimum contact hours}. This KPI has an FPE of 304.8, it has 45.4 interesting days on average, and has a correlation of 0.83. These values are probably a bit lower in reality due discussion points D12, discussed in \cref{sec:discussion}, but it is still in first place on all metrics by far.
As a secondary recommendation, we point to the \textit{teachers: maximum contact hours} KPI. The \textit{students: maximum contact hours} KPI would initially be the second recommendation due to its similar and higher metric values, but there's one important detail about that KPI. The Twents Educational Model (TEM) lets students have eight-hour-days often, so the KPI should actually change to allow for eight contact hours per day (as six contact hours per day is the current KPI).
Furthermore, we happen to know that teachers have the general opinion that their workload is too high\footnote{Karim was in the university council during the academic year 2015-2016. During this year, the results of the employee survey ("medewerkersonderzoek" in Dutch) included the teachers' opinion that the workload was too high. This was also one of the conclusions of many of the TEM-evaluations (which are available internally at the UT).}~\cite{medewerkersonderzoek2015}. Even though discussion point D18 has very much influence on the result, we still believe that decreasing the teacher's amount of working hours will have a strong, indirect effect on the NSE score.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{future research}
% data extraction
The first point of focus for future research should be improving the reliability of the data. The data owners explicitly argued that there might be errors in the data extraction process (as mentioned in \cref{sec:discussion}), so resolving these errors and ensuring the quality of the data is a good and necessary next step.

% focus on KPI
When the quality of the data has improved, further research could zoom in on the KPI analysis. The overview our research has provided is a good first step to improve the scheduling process, but the validity of our results can only be properly tested by examining each KPI in more detail.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% The bibliography is stored in an external database file
% in the BibTeX format (file_name.bib).  The bibliography is
% created by the following command and it will appear in this
% position in the document. You may, of course, create your
% own bibliography by using thebibliography environment as in
%
% \begin{thebibliography}{12}
% ...
% \bibitem{itemreference} D. E. Knudsen.
% {\em 1966 World Bnus Almanac.}
% {Permafrost Press, Novosibirsk.}
% ...
% \end{thebibliography}

% Here's where you specify the bibliography style file.
% The full file name for the bibliography style file 
% used for an ASME paper is asmems4.bst.
\bibliographystyle{asmems4}

% Here's where you specify the bibliography database file.
% The full file name of the bibliography database for this
% article is asme2e.bib. The name for your database is up
% to you.
\bibliography{asme2e}

\clearpage

\appendix
\section{Key performance indicators}\label{app:kpis}
The lists of \cref{tab:kpis_saxion,tab:kpis_ut} contain the key performance indicators of the respective institution as we received them at the start of the project.
\begin{table}[h]
    \centering
    \caption{KPIs of Hogeschool Saxion}
    \label{tab:kpis_saxion}
    \begin{tabular}{p{15em} p{34em}}
        \hline
        \textbf{Label} & \textbf{KPI} \\\hline\hline
        students: day length & Fulltime timetable is between 8.30 and 18.00, part-time timetable is between 8.30 and 21.30. \\
        students: contact days & The contact hours/classes are spread over a maximum of four days {[}per week{]}. The remaining day is meant for practical exercises, self-study and projects. \\
        students: minimum hours & A minimum of 4 college hours on any day. \\
        students: maximum hours & Fulltime is max. 8 lesson hours. Part-time is max. 10 lesson hours. \\
        students: maximum free period & A student can, on any day, have a maximum of 2 free hours in 1 block (planned shoulder-to-shoulder). Free hours meaning: not scheduled lesson hour. \\
        students: maximum free hours & A student can, on any day, have a maximum of 3 free hours in total. Free hours meaning: not scheduled lesson hour. \\
        students: maximum condensed hours & After a maximum of 6 condensed/serried lesson hours a free hour is scheduled as a pause. \\
        students: dinner time & For part-time students a moment (time/block/hour) is scheduled for dinner; a minimum of 45 minutes between 16.30 – 18.30. \\
        students: location & At maximum once a day change of building. \\
        teachers: day length & \begin{tabular}[c]{@{}l@{}}Fulltime timetable is between 8.30 – 18.00,\\ part-time timetable can differ in each academy between 8.30 – 21.30.\end{tabular} \\
        teachers: clock hours & The maximum working day counts 10 clock hours (according to regulations). \\
        teachers: next free morning & In part-time education, after 20.30, plan the next morning off. \\
        teachers: maximum lesson hours & Maximum of 8 lesson hours on any day, of which a maximum of 6 for (plenary) lectures \\
        teachers: maximum condensed hours & After a maximum of 6 condensed/serried lesson hours a free hour is scheduled as a pause. \\
        teachers: dinner time & For teachers teaching part-time students a free moment is scheduled for dinner; a minimum of 45 minutes between 16.30 – 18.30. \\
        teachers: location (cities) & Teachers may travel between different locations at max. once a day (establishments in Enschede, Deventer, Apeldoorn, Hengelo). \\
        teachers: location (buildings) & Maximum of 2 times a day for travelling between buildings at the same location. \\
    \end{tabular}
\end{table}
\clearpage

\begin{table}[h]
    \centering
    \caption{KPIs of the University of Twente}\label{tab:kpis_ut}
    \begin{tabular}{p{15em} p{34em}}
        \hline
        \textbf{Label} & \textbf{KPI} \\\hline\hline
        students: minimum contact hours & Students have a minimum of 4 contact hours on any day. \\
        students: maximum contact hours & Students have a maximum of 6 contact hours on any day. \\
        students: maximum free period & Students have a maximum of 2 free hours in 1 series on any day. \\
        students: maximum hours & The timetable of students have a maximum of 11 college hours on any day. This means 8:15 clock hours, which is the time between start of the first college and the end of the last college on any day). \\
        students: next free morning & If a student has a class at the 11th and 12th college hour, then that student has no class at the 1st and 2nd college hour the next day. \\
        students: friday evening & At Fridays there are no evening classes. \\
        teachers: maximum contact hours & A teacher has a maximum of 8 contact hours per day. \\
        teachers: next free morning & If a teacher has a class at the 11th and 12th college hour, then that teacher has no class at the 1st and 2nd college hour the next day. \\
        occupation & Rooms must have an occupation of at least 70\%. Occupation is defined as follows: occupying a space (room) by the timetabling process during educational weeks. 
    \end{tabular}
\end{table}

\end{document}
