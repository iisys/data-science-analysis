-- Copy unique activities per class.
-- unique by: datum, start, end, class
TRUNCATE tt_sax.activities;
INSERT
    tt_sax.activities (aid, datum, start, end, duration, next_class_activity, next_teacher_activity, name, remarks, type, bisoncode, bisoncodes, class, teacher)
SELECT
    aid,
    datum,
    start,
    end,
    TIMEDIFF(end, start) AS duration,
    TIMEDIFF(COALESCE((SELECT   a1.start
                        FROM    tt_sax.activities_orig a1
                        WHERE   a1.start >= a.end
                          AND   a.datum = a1.datum
                          AND   a.class = a1.class
                          AND   a1.class IS NOT NULL
                        ORDER BY a1.start
                        LIMIT   1),
                    NULL),
            a.end) AS next_class_activity,
    TIMEDIFF(COALESCE((SELECT   a1.start
                        FROM    tt_sax.activities_orig a1
                        WHERE   a1.start >= a.end
                          AND   a.datum = a1.datum
                          AND   a.teacher = a1.teacher
                          AND   a1.teacher IS NOT NULL
                        ORDER BY a1.start
                        LIMIT   1),
                    NULL),
            a.end) AS next_teacher_activity,
    GROUP_CONCAT(DISTINCT name		ORDER BY name      SEPARATOR ', ') AS name,
    GROUP_CONCAT(DISTINCT remarks	ORDER BY remarks   SEPARATOR ', ') AS remarks,
    activity AS type,
    bisoncode,
    GROUP_CONCAT(DISTINCT bisoncode	ORDER BY bisoncode SEPARATOR  ',') AS bisoncodes,
    class,
    teacher
FROM
	tt_sax.activities_orig a
GROUP BY
	class, teacher, datum, start, end;
	


-- Copy date data.
TRUNCATE tt_sax.dates;
INSERT INTO
	tt_sax.dates (datum, schoolyear, quartile, quartile_week, lessonweek)
SELECT DISTINCT
	datum,
	CAST(SUBSTRING(schoolyear FROM 1 FOR 4) AS UNSIGNED) AS schoolyear,
	quartile,
	CAST(SUBSTRING(quartile_week FROM LOCATE('.', quartile_week) + 1) AS UNSIGNED) AS quartile_week,
	lessonweek
FROM
	tt_sax.activities_orig
WHERE
      schoolyear IS NOT NULL
  AND quartile IS NOT NULL
  AND quartile_week IS NOT NULL
  AND lessonweek IS NOT NULL;


-- Copy location data (duration: 1 minute).
TRUNCATE tt_sax.activities_locations;
INSERT INTO
    tt_sax.activities_locations (aid, city, room, wing, building)
SELECT DISTINCT
    a.aid                                                                                           AS aid,
    IF(room IN ('ROC', 'ROC gymzaal'),                   'D',
    IF(SUBSTRING(room,1,1) IN ('A','B','C','D','X'),     'D',
    IF(SUBSTRING(room,1,1) IN ('K'),                     'A',   -- this includes KC rooms
    IF(SUBSTRING(room,1,1) IN ('E','F','H','S','W','G','N','O','P','Q','I','R','M'), 'E',
        o.place
    ))))                                                                                            AS place,
    IF(o.room REGEXP '^[a-z]-?[0-9]\\.[0-9]{1,3}[a-z]?$', REPLACE(o.room, '.', ''), 
    IF(o.room REGEXP '^I[0-9]{4}ITC$',                    REPLACE(o.room, 'ITC', ''),
    IF(o.room REGEXP '^KC\\.[0-9]{2}$',                   REPLACE(o.room, ',', ''),
        o.room
    )))                                                                                             AS room,
    IF(room IN ('ROC', 'ROC gymzaal'),                   'ROC',
    IF(room REGEXP '^[a-z]-?[0-9]+',                      SUBSTR(room FROM 1 FOR 1),
    IF(SUBSTR(room,1,2) = 'KC',                           'KC',
        NULL
    )))                                                                                             AS wing,
    -- Deventer: Handelskade (A, B, C, D) / ROC (ROC) / SVB (X)
    IF(SUBSTRING(room,1,1) IN ('A','B','C','D'),    'Handelskade',
    IF(SUBSTRING(room,1,1) = 'X',                   'SVB',
    IF(room IN ('ROC','ROC gymzaal'),               room,
    -- Apeldoorn: Spoorstraat (K)
    IF(SUBSTRING(room,1,1) IN ('K'),                'Spoorstraat',
    -- Enschede: Ko Wieringa (E, F, H, S, W) / Epy Drost (G) / Edith Stein (N, O, P, Q) / ITC (I) / Maere (M)
    IF(place = 'E' AND room = 'ext.agz',            room,
    IF(SUBSTRING(room,1,1) IN ('E','F','H','S','W'),'Ko Wieringa',
    IF(SUBSTRING(room,1,1) IN ('N','O','P','Q'),    'Edith Stein',
    IF(SUBSTRING(room,1,1) IN ('G'),                'Epy Drost',
    IF(SUBSTRING(room,1,1) IN ('R'),                'Randstad',     -- 
    IF(SUBSTRING(room,1,1) IN ('I'),                'ITC',
    IF(SUBSTRING(room,1,1) IN ('M'),                'Maere',
        NULL
    ))))))))))) AS building
FROM
    tt_sax.activities a
LEFT JOIN
    tt_sax.activities_orig o
ON  a.datum = o.datum 	AND
    a.start = o.start 	AND
    a.end   = o.end		AND
    a.class = o.class   AND
    a.teacher = o.teacher
WHERE
	o.room IS NOT NULL;

-- Copy class data
TRUNCATE tt_sax.classes;
INSERT INTO
	tt_sax.classes (class, timetype, educode, educodes, academy, academies)
SELECT
	class,
	IF(    GROUP_CONCAT(DISTINCT educode SEPARATOR ',') REGEXP ' vt', 'V',
		IF(GROUP_CONCAT(DISTINCT educode SEPARATOR ',') REGEXP ' dt', 'D',
        IF(class REGEXP '^EEL[0-9]', 'D',
		IF(class REGEXP '[0-9]{1}V$',	'V',
		IF(class REGEXP '[0-9]{1}V.*$', 'V',
		IF(class REGEXP '[0-9]{1}D$', 	'D',
		IF(class REGEXP '[0-9]{1}D.*$', 'D',
		IF(class REGEXP '^[a-z]{3,4}[0-9]{1}$', 'V',
		IF(class REGEXP '[0-9]{1}[a-z]{1}$', 'V',
            NULL
        ))))))))) AS timetype,
    educode,
    GROUP_CONCAT(DISTINCT educode ORDER BY educode SEPARATOR ',') AS educodes,
    academy,
    GROUP_CONCAT(DISTINCT academy ORDER BY academy SEPARATOR ',') AS academies
FROM
	tt_sax.activities_orig
WHERE
	class IS NOT NULL
GROUP BY
	class;