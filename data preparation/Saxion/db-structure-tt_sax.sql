-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: localhost    Database: tt_sax
-- ------------------------------------------------------
-- Server version	5.7.17-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `activities`
--

DROP TABLE IF EXISTS `activities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `activities` (
  `aid` int(10) unsigned NOT NULL,
  `datum` date DEFAULT NULL,
  `start` time DEFAULT NULL,
  `end` time DEFAULT NULL,
  `duration` time DEFAULT NULL,
  `next_class_activity` time DEFAULT NULL,
  `next_teacher_activity` time DEFAULT NULL,
  `name` text,
  `remarks` text,
  `type` char(1) DEFAULT NULL,
  `bisoncode` varchar(45) DEFAULT NULL,
  `bisoncodes` text,
  `class` varchar(20) DEFAULT NULL,
  `teacher` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`aid`),
  KEY `idx_datum` (`datum`),
  KEY `idx_activities_perclass` (`datum`,`start`,`end`,`class`),
  KEY `idx_activities_perteacher` (`datum`,`start`,`end`,`teacher`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `activities_locations`
--

DROP TABLE IF EXISTS `activities_locations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `activities_locations` (
  `aid` int(11) NOT NULL,
  `city` char(1) DEFAULT NULL,
  `building` varchar(45) DEFAULT NULL,
  `wing` varchar(3) DEFAULT NULL,
  `room` varchar(100) NOT NULL,
  PRIMARY KEY (`aid`,`room`),
  KEY `idx_activities_locations_aid` (`aid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `activities_orig`
--

DROP TABLE IF EXISTS `activities_orig`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `activities_orig` (
  `aid` int(10) unsigned NOT NULL,
  `datum` date DEFAULT NULL,
  `start` time DEFAULT NULL,
  `end` time DEFAULT NULL,
  `academy` varchar(10) DEFAULT NULL,
  `place` char(1) DEFAULT NULL,
  `class` varchar(20) DEFAULT NULL,
  `educode` varchar(45) DEFAULT NULL,
  `bisoncode` varchar(45) DEFAULT NULL,
  `name` text,
  `teacher` varchar(10) DEFAULT NULL,
  `namefull` varchar(45) DEFAULT NULL,
  `room` varchar(100) DEFAULT NULL,
  `remarks` text,
  `calendar_week` tinyint(3) unsigned DEFAULT NULL,
  `quartile_week` varchar(8) DEFAULT NULL,
  `quartile` tinyint(3) unsigned DEFAULT NULL,
  `lessonweek` tinyint(3) unsigned DEFAULT NULL,
  `activity` char(1) DEFAULT NULL,
  `schoolyear` varchar(9) DEFAULT NULL,
  PRIMARY KEY (`aid`),
  KEY `idx_datum` (`datum`),
  KEY `idx_activities_perclass` (`datum`,`start`,`end`,`class`),
  KEY `idx_activities_perteacher` (`datum`,`start`,`end`,`teacher`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `classes`
--

DROP TABLE IF EXISTS `classes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `classes` (
  `class` varchar(20) NOT NULL,
  `timetype` char(1) DEFAULT NULL,
  `educode` varchar(45) DEFAULT NULL,
  `educodes` text,
  `academy` varchar(10) DEFAULT NULL,
  `academies` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`class`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `dates`
--

DROP TABLE IF EXISTS `dates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dates` (
  `datum` date NOT NULL,
  `schoolyear` year(4) DEFAULT NULL,
  `quartile` tinyint(4) DEFAULT NULL,
  `quartile_week` tinyint(4) DEFAULT NULL,
  `lessonweek` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`datum`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping routines for database 'tt_sax'
--
/*!50003 DROP FUNCTION IF EXISTS `CORRECT_COUNT` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `CORRECT_COUNT`(
	s 	  VARCHAR(255),
    delim VARCHAR(10)
) RETURNS int(11)
RETURN
	-- Maximum of 20 substrings!
	IFNULL(1 / COUNT_STR(s, SPLIT_STR(s, delim, 1)), 0) +
    IFNULL(1 / COUNT_STR(s, SPLIT_STR(s, delim, 2)), 0) +
    IFNULL(1 / COUNT_STR(s, SPLIT_STR(s, delim, 3)), 0) +
    IFNULL(1 / COUNT_STR(s, SPLIT_STR(s, delim, 4)), 0) +
    IFNULL(1 / COUNT_STR(s, SPLIT_STR(s, delim, 5)), 0) +
    IFNULL(1 / COUNT_STR(s, SPLIT_STR(s, delim, 6)), 0) +
    IFNULL(1 / COUNT_STR(s, SPLIT_STR(s, delim, 7)), 0) +
    IFNULL(1 / COUNT_STR(s, SPLIT_STR(s, delim, 8)), 0) +
    IFNULL(1 / COUNT_STR(s, SPLIT_STR(s, delim, 9)), 0) +
    IFNULL(1 / COUNT_STR(s, SPLIT_STR(s, delim, 10)), 0) +
    IFNULL(1 / COUNT_STR(s, SPLIT_STR(s, delim, 11)), 0) +
    IFNULL(1 / COUNT_STR(s, SPLIT_STR(s, delim, 12)), 0) +
    IFNULL(1 / COUNT_STR(s, SPLIT_STR(s, delim, 13)), 0) +
    IFNULL(1 / COUNT_STR(s, SPLIT_STR(s, delim, 14)), 0) +
    IFNULL(1 / COUNT_STR(s, SPLIT_STR(s, delim, 15)), 0) +
    IFNULL(1 / COUNT_STR(s, SPLIT_STR(s, delim, 16)), 0) +
    IFNULL(1 / COUNT_STR(s, SPLIT_STR(s, delim, 17)), 0) +
    IFNULL(1 / COUNT_STR(s, SPLIT_STR(s, delim, 18)), 0) +
    IFNULL(1 / COUNT_STR(s, SPLIT_STR(s, delim, 19)), 0) +
    IFNULL(1 / COUNT_STR(s, SPLIT_STR(s, delim, 20)), 0) ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `COUNT_STR` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `COUNT_STR`(
	haystack TEXT, 
    needle VARCHAR(255)
) RETURNS int(11)
    DETERMINISTIC
RETURN 
	IF(needle IS NULL OR needle = '',
		0,
		ROUND(
			(
			CHAR_LENGTH(haystack) - CHAR_LENGTH(REPLACE(haystack, needle, ""))
			) 
			/
			CHAR_LENGTH(needle)
		)
	) ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `SPLIT_STR` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `SPLIT_STR`(
	x VARCHAR(255),
	delim VARCHAR(12),
	pos INT
) RETURNS varchar(255) CHARSET utf8
RETURN 
	REPLACE(
		SUBSTRING(
			SUBSTRING_INDEX(x, delim, pos), 
            CHAR_LENGTH(SUBSTRING_INDEX(x, delim, pos -1) ) + 1), 
		delim, 
        '') ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-02-01 20:31:04
