-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: localhost    Database: tt_ut
-- ------------------------------------------------------
-- Server version	5.7.17-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `activities`
--

DROP TABLE IF EXISTS `activities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `activities` (
  `aid` int(10) NOT NULL,
  `datum` date DEFAULT NULL,
  `start` time DEFAULT NULL,
  `end` time DEFAULT NULL,
  `duration_orig` time DEFAULT NULL,
  `duration` time DEFAULT NULL,
  `next_class_activity` time DEFAULT NULL,
  `teacher` varchar(45) DEFAULT NULL,
  `type` varchar(20) DEFAULT NULL,
  `name` varchar(300) DEFAULT NULL,
  `description` varchar(300) DEFAULT NULL,
  `programme` varchar(100) DEFAULT NULL,
  `location` varchar(100) DEFAULT NULL,
  `location_orig` varchar(100) DEFAULT NULL,
  `location_size` bigint(20) DEFAULT NULL,
  `hostkey1` varchar(100) DEFAULT NULL,
  `hostkey2` varchar(100) DEFAULT NULL,
  KEY `idx_activities_perprogramme` (`datum`,`start`,`end`,`programme`),
  KEY `idx_activities_datum_hostkey1_hostkey2` (`datum`,`hostkey1`,`hostkey2`),
  KEY `idx_activities_aid` (`aid`),
  KEY `idx_activities_hostkey1_hostkey2_academicyear` (`hostkey1`,`hostkey2`),
  KEY `idx_activities_datum` (`datum`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `activities_orig`
--

DROP TABLE IF EXISTS `activities_orig`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `activities_orig` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `naam_activiteit` varchar(300) DEFAULT NULL,
  `beschrijving_activiteit` varchar(300) DEFAULT NULL,
  `hostkey1` varchar(100) DEFAULT NULL,
  `hostkey2` varchar(100) DEFAULT NULL,
  `activiteitstype` varchar(20) DEFAULT NULL,
  `datum` date DEFAULT NULL,
  `start` time DEFAULT NULL,
  `eind` time DEFAULT NULL,
  `grootte` bigint(20) DEFAULT NULL,
  `zaal_activiteit` varchar(100) DEFAULT NULL,
  `programme` varchar(100) DEFAULT NULL,
  `academicyear` year(4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_activities_orig_datum_start_eind` (`datum`,`start`,`eind`),
  KEY `idx_activities_orig_hostkey1_hostkey2_datum_start_eind` (`hostkey1`,`hostkey2`,`datum`,`start`,`eind`),
  KEY `idx_activities_orig_programme_datum_start_eind` (`programme`,`datum`,`start`,`eind`),
  KEY `idx_activities_orig_hostkey1_hostkey2_academicyear` (`hostkey1`,`hostkey2`,`academicyear`),
  KEY `idx_activities_orig_datum_start_eind_programme` (`datum`,`start`,`eind`,`programme`)
) ENGINE=InnoDB AUTO_INCREMENT=203951 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `dates`
--

DROP TABLE IF EXISTS `dates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dates` (
  `datum` date NOT NULL,
  `academicyear` year(4) DEFAULT NULL,
  `quartile` tinyint(3) unsigned DEFAULT NULL,
  `quartile_week` tinyint(3) unsigned DEFAULT NULL,
  PRIMARY KEY (`datum`),
  KEY `idx_dates_academicyear_quartile_datum` (`academicyear`,`quartile`,`datum`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `docenten_orig`
--

DROP TABLE IF EXISTS `docenten_orig`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `docenten_orig` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cursus` varchar(45) DEFAULT NULL,
  `cursusnaam` varchar(45) DEFAULT NULL,
  `collegejaar` year(4) DEFAULT NULL,
  `medewerker` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_docenten_orig_cursus_collegejaar` (`cursus`,`collegejaar`)
) ENGINE=InnoDB AUTO_INCREMENT=24442 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `locations`
--

DROP TABLE IF EXISTS `locations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `locations` (
  `location` varchar(50) NOT NULL,
  `building` varchar(20) DEFAULT NULL,
  `capacity` smallint(6) DEFAULT NULL,
  `weight` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`location`),
  KEY `idx_locations_building` (`building`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping routines for database 'tt_ut'
--
/*!50003 DROP FUNCTION IF EXISTS `correct_for_breaktime` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `correct_for_breaktime`(
	duration TIME,
    start	 TIME,
    end		 TIME
) RETURNS time
RETURN SUBTIME(duration, GET_BREAKTIME(start, end)) ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `get_breaktime` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `get_breaktime`(
	start TIME, 
	end TIME
) RETURNS time
RETURN 
	sec_to_time(
		time_to_sec('00:15:00') * (
			if(start <= '09:30:00' and end >= '09:45:00', 1, 0) +
			if(start <= '10:30:00' and end >= '10:45:00', 1, 0) +
			if(start <= '11:30:00' and end >= '11:45:00', 1, 0) +
			if(start <= '12:30:00' and end >= '13:45:00', 5, 0) +
			if(start <= '14:30:00' and end >= '14:45:00', 1, 0) +
			if(start <= '15:30:00' and end >= '15:45:00', 1, 0) +
			if(start <= '16:30:00' and end >= '16:45:00', 1, 0) +
			if(start <= '17:30:00' and end >= '18:45:00', 5, 0) +
			if(start <= '19:30:00' and end >= '19:45:00', 1, 0) +
			if(start <= '20:30:00' and end >= '20:45:00', 1, 0) +
			if(start <= '21:30:00' and end >= '21:45:00', 1, 0)
		)
	) ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `loc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `loc`(
	location VARCHAR(255)
) RETURNS varchar(255) CHARSET utf8
RETURN
	REPLACE(
	REPLACE(
	REPLACE(location,
	'ZZTCP ', ''),
    'ZZ ',    ''),
    '/T',     '') ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `SPLIT_STR` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `SPLIT_STR`(
	x VARCHAR(255),
	delim VARCHAR(12),
	pos INT
) RETURNS varchar(255) CHARSET utf8
RETURN 
	REPLACE(
		SUBSTRING(
			SUBSTRING_INDEX(x, delim, pos), 
            CHAR_LENGTH(SUBSTRING_INDEX(x, delim, pos -1) ) + 1), 
		delim, 
        '') ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-02-01 20:31:54
