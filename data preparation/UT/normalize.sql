-- Normalize data.
-- -- Add derived data: the study programme
-- -- Copy to other table with more derived data: duration and next activity for the same programme
-- -- -- I leave out all the entries without date and time
SET @coursecode         = '^[0-9]{8,9}[a-z]?(/[0-9]{9}){0,2}$|^ITC [0-9]{4}',
    @syllabuscode       = '^#SPLUS|^ITC\-[a-z]{2,4}',
    @unknowncoursecode  = '^x{2,8}$|^[0-9]{4}(000\\?|x{5}|\\?{4})?$|^[0-9]{8}$|^-$|^[0-9]{4}\\.{3,}$|^opstap$|^kmkk$',
    @noneducation       = '^BOEK$|^ONO$|^ZZ.*$',
    @education          = '^ASM$|^COL$|^CQ$|^D?TOETS$|^EXC$|^HC$|^OVO$|^PJB$|^PJO$|^PRA$|^PRS$|^RESP?$|^WC$|^WG$|^ZGB$|^ZMB$',
    @conctacteducation  = '^ASM$|^COL$|^CQ$|^D?TOETS$|^EXC$|^HC$|^OVO$|^PJB$|^PRA$|^PRS$|^RESP?$|^WC$|^WG$|^ZMB$';
    
UPDATE  tt_ut.activities_orig
SET     academicyear = YEAR(datum) - IF(MONTH(datum) < 9, 1, 0),
        programme = 
            IF(activiteitstype  REGEXP @noneducation,                                   NULL,     -- irrelevant
            IF(hostkey2         REGEXP @coursecode OR hostkey2 REGEXP @syllabuscode,    hostkey2,
            IF(hostkey2         REGEXP @unknowncoursecode,                              NULL,     -- unknown
                NULL
            )));

-- Approximate runtime: 3.5 hours
TRUNCATE tt_ut.activities;
INSERT INTO
    tt_ut.activities (aid, datum, start, end, duration_orig, duration, next_class_activity, teacher, type, name, description, programme, location, location_orig, location_size, hostkey1, hostkey2)
SELECT
        a.id                                        AS aid,
        datum, start, eind                          AS end,
        TIMEDIFF(eind, start)                       AS duration_orig,
        tt_ut.correct_for_breaktime(
            TIMEDIFF(eind, start), start, eind
        )                                           AS duration,
        IF(activiteitstype REGEXP @education,
        TIMEDIFF(COALESCE((SELECT   b.start
                            FROM    tt_ut.activities_orig b
                            WHERE   b.start >= a.eind
                              AND   a.datum  = b.datum
                              AND   a.programme = b.programme
                              AND   b.programme IS NOT NULL
                              AND   b.activiteitstype REGEXP @education
                            ORDER BY b.start
                            LIMIT   1),
                        NULL),
                eind), NULL)                        AS next_class_activity,
        d.medewerker                                AS teacher,
        activiteitstype                             AS type,
        naam_activiteit                             AS name,
        beschrijving_activiteit                     AS description,
        programme                                   AS programme,
        tt_ut.LOC(zaal_activiteit)                  AS location,
        zaal_activiteit                             AS location_orig,
        grootte                                     AS location_size,
        hostkey1,
        hostkey2        
FROM    tt_ut.activities_orig a
LEFT JOIN   tt_ut.docenten_orig d
    ON      (a.hostkey1 = d.cursus OR a.hostkey2 = d.cursus)
        AND d.collegejaar = a.academicyear
WHERE   datum IS NOT NULL 
  AND   start IS NOT NULL
  AND   eind  IS NOT NULL
ORDER BY programme, datum, start;

UPDATE
    tt_ut.activities
SET
    location = 'GY 1.79 Inform'
WHERE
    location = 'GY 1.79 Inform (gesloten ruimte)';


-- Get location data.
TRUNCATE tt_ut.locations;
INSERT INTO
    tt_ut.locations (location, building, capacity, weight)
SELECT 
        location                                                                                                    AS location,
        IF(location REGEXP '^[a-z]{2,3} [0-9a-z -\\.\\(\\)/+]+$', tt_ut.SPLIT_STR(location, ' ', 1),
        IF(location IN ('EXTERN','THERM'),                        location,
            NULL
        ))                                                                                                          AS building,
        MAX(location_size)                                                                                          AS capacity,
        IF(SUBSTR(location FROM 1 FOR 2) IN ('VR ', 'GY ') OR 
           location IN ('SC', 'EXTERN', 'THERM'),                0,
        IF(LOCATE('+', location),                                2,
            1
        ))                                                                                                          AS weight
FROM  tt_ut.activities
WHERE location IS NOT NULL
GROUP BY location;



-- normalize dates
TRUNCATE tt_ut.dates;
INSERT INTO
    tt_ut.dates (datum, academicyear)
SELECT 
    datum,
    YEAR(datum) - IF(MONTH(datum) < 9, 1, 0)        AS academicyear
FROM
    tt_ut.activities
GROUP BY 
    datum;

UPDATE tt_ut.dates SET quartile = 1 WHERE datum BETWEEN '2013-09-02' AND '2013-11-08';
UPDATE tt_ut.dates SET quartile = 2 WHERE datum BETWEEN '2013-11-11' AND '2014-01-31';
UPDATE tt_ut.dates SET quartile = 3 WHERE datum BETWEEN '2014-02-03' AND '2014-04-18';
UPDATE tt_ut.dates SET quartile = 4 WHERE datum BETWEEN '2014-04-21' AND '2014-07-04';
UPDATE tt_ut.dates SET quartile = 1 WHERE datum BETWEEN '2014-09-01' AND '2014-11-07';
UPDATE tt_ut.dates SET quartile = 2 WHERE datum BETWEEN '2014-11-10' AND '2015-01-30';
UPDATE tt_ut.dates SET quartile = 3 WHERE datum BETWEEN '2015-02-02' AND '2015-04-17';
UPDATE tt_ut.dates SET quartile = 4 WHERE datum BETWEEN '2015-04-20' AND '2015-07-03';
UPDATE tt_ut.dates SET quartile = 1 WHERE datum BETWEEN '2015-08-31' AND '2015-11-06';
UPDATE tt_ut.dates SET quartile = 2 WHERE datum BETWEEN '2015-11-09' AND '2016-01-29';
UPDATE tt_ut.dates SET quartile = 3 WHERE datum BETWEEN '2016-02-01' AND '2016-04-15';
UPDATE tt_ut.dates SET quartile = 4 WHERE datum BETWEEN '2016-04-18' AND '2016-07-01';
UPDATE tt_ut.dates SET quartile = 1 WHERE datum BETWEEN '2016-09-05' AND '2016-11-11';
UPDATE tt_ut.dates SET quartile = 2 WHERE datum BETWEEN '2016-11-14' AND '2017-02-03';
UPDATE tt_ut.dates SET quartile = 3 WHERE datum BETWEEN '2017-02-06' AND '2017-04-21';
UPDATE tt_ut.dates SET quartile = 4 WHERE datum BETWEEN '2017-04-24' AND '2017-07-07';

REPLACE INTO
    tt_ut.dates (datum, academicyear, quartile, quartile_week)
SELECT 
    datum, academicyear, quartile,
    (
    SELECT  1 + COUNT(DISTINCT WEEK(d2.datum))
    FROM    tt_ut.dates d2
    WHERE   d1.academicyear = d2.academicyear AND d1.quartile = d2.quartile
            AND WEEK(d2.datum) != WEEK(d1.datum)
            AND d2.datum < d1.datum
    )                                               AS quartile_week
FROM    
    tt_ut.dates d1
WHERE
    d1.quartile IS NOT NULL;
