-- Rooms: occupation during educational weeks.

-- Occupation calculation: total occupation time divided by the total available time.
-- The available time is calculated by taking 8 college hours per day, 5 days per week, 8 weeks per quartile (10 minus 2 exam weeks), 4 quartiles per academic year, and 3 years
SET @days = 5*8*4*3,
    @available_time = @days * TIME_TO_SEC('06:00:00'); -- = 8 * 45 minutes of college hours

-- Approximate runtime: 16 seconds.
SELECT
    location,
    SUM(TIME_TO_SEC(occupation_time)) / @available_time * 100 AS occupancy
FROM (
    -- location per day
    SELECT
            location,
            datum,
            SEC_TO_TIME(SUM(TIME_TO_SEC(duration))) AS occupation_time
    FROM (
        -- location per timeslot
        SELECT DISTINCT
                a.location,
                a.datum, 
                a.start, 
                a.end,
                a.duration
        FROM	tt_ut.activities a
        INNER JOIN tt_ut.dates   d      ON a.datum    = d.datum
        LEFT  JOIN tt_ut.locations l    ON a.location = l.location    
        WHERE	a.location IS NOT NULL
          AND   d.academicyear BETWEEN 2013 and 2015
          AND   d.quartile_week <= 11                   -- 11 weeks to account for some holidays in which activities are scheduled.
          AND   l.weight > 0
      ) a
    GROUP BY    location, datum
  ) b
GROUP BY    location
