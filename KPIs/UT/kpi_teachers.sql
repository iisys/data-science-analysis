-- Teachers: maximum of 8 contact hours per day
-- Teachers: if class ends later than 20:45, the first two hours of the next day should be free
SET @contacteducation     = '^ASM$|^COL$|^CQ$|^D?TOETS$|^EXC$|^HC$|^OVO$|^PJB$|^PRA$|^PRS$|^RESP?$|^WC$|^WG$|^ZMB$',
    @morning_end          = '10:30:00';

-- Approximate runtime: 20 minutes.
SELECT
    teacher,
    COUNT(0)                                                    AS `scheduled days`,
    COUNT(CASE WHEN too_many_contact_hours THEN 1 END)          AS `days with too many contact hours`,
    COUNT(CASE WHEN too_busy_next_morning  THEN 1 END)          AS `late nights with too busy next mornings`
FROM (
    SELECT
        datum,
        teacher,
        
        COALESCE(SEC_TO_TIME(SUM(
            CASE WHEN type REGEXP @contacteducation THEN TIME_TO_SEC(duration) END
        )), '00:00:00') > '06:00:00'                            AS too_many_contact_hours, --  =  8 college hours

        MAX(end) > '20:45:00'                                   AS late_night,
        IF(MAX(end) > '20:45:00',
            (
                SELECT  COUNT(0)
                FROM    tt_ut.activities a1
                WHERE   a.teacher = a1.teacher
                  AND   a1.datum = DATE_ADD(a.datum, INTERVAL 1 DAY)
                  AND   a1.start < @morning_end
            ) > 0, NULL)                                        AS too_busy_next_morning
      
    FROM (
        -- activities per teacher per timeslot
        SELECT      teacher, 
                    datum, start, end, duration,
                    type
        FROM        tt_ut.activities
        WHERE       teacher IS NOT NULL
        GROUP BY    datum, start, end, teacher
        ) a
    GROUP BY datum, teacher
  ) a
GROUP BY teacher