-- Students: minimum of 4 contact hours per day
-- Students: maximum of 6 contact hours per day
-- Students: maximum of 2 free hours in 1 series per day
-- Students: maximum of 11 college hours per day, so a maximum of 8:15 clock hours between the day's start and end
-- Students: if class ends later than 18:45, the first two hours of the next day should be free
-- Students: no class after 17:30 on friday
SET @contacteducation     = '^ASM$|^COL$|^CQ$|^D?TOETS$|^EXC$|^HC$|^OVO$|^PJB$|^PRA$|^PRS$|^RESP?$|^WC$|^WG$|^ZMB$',
    @morning_end          = '10:30:00',
    @friday_evening_start = '17:45:00';

-- Approximate runtime: 2.5 minutes.
SELECT
    programme,
    COUNT(0)                                                        AS `scheduled days`,
    COUNT(CASE WHEN too_few_contact_hours  THEN 1 END)              AS `days with too few contact hours`,
    COUNT(CASE WHEN too_many_contact_hours THEN 1 END)              AS `days with too many contact hours`,
    COUNT(CASE WHEN too_long_free_period   THEN 1 END)              AS `days with too long free period`,
    COUNT(CASE WHEN too_long_day_duration  THEN 1 END)              AS `days that last too long`,
    COUNT(CASE WHEN too_busy_next_morning  THEN 1 END)              AS `late nights with too busy next mornings`,
    COUNT(CASE WHEN friday_evening_class   THEN 1 END)              AS `friday evenings`
FROM (
    SELECT
        datum,
        programme,
        COALESCE(SEC_TO_TIME(SUM(
            CASE WHEN type REGEXP @contacteducation THEN TIME_TO_SEC(duration) END
        )), '00:00:00') < '03:00:00'                                                AS too_few_contact_hours,  --  =  4 college hours
        COALESCE(SEC_TO_TIME(SUM(
            CASE WHEN type REGEXP @contacteducation THEN TIME_TO_SEC(duration) END
        )), '00:00:00') > '04:30:00'                                                AS too_many_contact_hours, --  =  6 college hours
        COALESCE(MAX(next_class_activity),                '00:00:00') > '01:45:00'  AS too_long_free_period,   --  =  2 college hours + 15 minute break time
        COALESCE(SEC_TO_TIME(SUM(TIME_TO_SEC(duration))), '00:00:00') > '08:15:00'  AS too_long_day_duration,  --  = 11 college hours
        MAX(end) > '20:45:00'                                                       AS late_night,
        IF(MAX(end) > '20:45:00', -- the KPI says 11th and 12th hour, so I assume the last two college hours of the day are targeted
            (
                SELECT  COUNT(0)
                FROM    tt_ut.activities a1
                WHERE   a.programme = a1.programme
                  AND   a1.datum = DATE_ADD(a.datum, INTERVAL 1 DAY)
                  AND   a1.start < @morning_end
            ) > 0, FALSE)                                                           AS too_busy_next_morning,
        DAYOFWEEK(datum) = 6 AND MAX(end) > @friday_evening_start                   AS friday_evening_class
            
    FROM (
        -- activities per programme per timeslot
        SELECT      *
        FROM        tt_ut.activities
        WHERE       programme IS NOT NULL
        GROUP BY    datum, start, end, programme
        ) a
    GROUP BY datum, programme
  ) a
GROUP BY programme