-- Teachers: 1 free college hour per 6 sequential college hours.
SET @max_condensed_hours = SEC_TO_TIME(6 * TIME_TO_SEC('00:45:00')); -- = 04:30:00 hours

-- Approximate runtime: 30 seconds.
SELECT
    teacher,
    COUNT(0)                                                AS `working days`,
    COUNT(CASE WHEN insufficient_break = 1 then 1 end)      AS `days without sufficient break`,
    COUNT(CASE WHEN insufficient_break = 0 then 1 end)      AS `days with sufficient break`
FROM (
    SELECT
        datum,
        teacher,
        IF(COUNT(0) = 1 AND MAX(duration) > @max_condensed_hours, 1,
        IF(COUNT(0) = 2 AND SEC_TO_TIME(SUM(CASE WHEN  IFNULL(next_teacher_activity,'00:00:00') <= '00:15:00'  THEN TIME_TO_SEC(duration) END)) > @max_condensed_hours, 1,
        IF(SEC_TO_TIME(SUM(TIME_TO_SEC(duration))) <= @max_condensed_hours, 0,
            NULL
        )))                                                 AS insufficient_break
    FROM (
        -- L-type activities per teacher per timeslot
        SELECT      *
        FROM        tt_sax.activities
        WHERE       teacher IS NOT NULL
          AND       type = 'L'
        GROUP BY    datum, start, end, teacher
        ) a
    GROUP BY datum, teacher
  ) a
GROUP BY teacher