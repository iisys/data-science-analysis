-- Students: full time is between 8:30 and 18:00.
-- Students: part time is between 8:30 and 21:30.
SET @irrelevant_student_activities = 'Toets|PlusProgramma|project|project|^Stage(activiteiten)?$|^Exam$|^Exam check$|Studie(loopbaan)? ?begeleiding|^SLB|^Workshop$|^Extern$|^Externe activiteit$|^Overleg of Vergadering$|^Honours$|^Voorlichting$|^Activiteit$';

-- Approximate runtime: 2 minutes.
SELECT 
    class,
    COUNT(0)                                                        AS 'scheduled college days',
    COUNT(CASE WHEN out_of_bounds = 1 THEN 1 END)                   AS 'out of bounds days',
    timetype
FROM (
    SELECT
        a.class,
        a.datum,
        c.timetype,
        MIN(a.start)                                        AS earliest_activity_start,
        MAX(a.end)                                          AS latest_activity_end,
        MIN(a.start) < '08:30:00' OR 
        c.timetype = 'D' AND MAX(a.end) > '21:30:00' OR 
        c.timetype = 'V' AND MAX(a.end) > '18:00:00'        AS out_of_bounds
    FROM        tt_sax.activities a
    INNER JOIN  tt_sax.classes c       ON a.class = c.class
    WHERE       a.type = 'L'
      AND       a.name    NOT REGEXP @irrelevant_student_activities
      AND       a.remarks NOT REGEXP @irrelevant_student_activities
    GROUP BY a.class, a.datum
) a
GROUP BY    
    class