-- Teachers: maximum of 10 clock hours per day.

-- Approximate runtime: 30 seconds
SELECT
    teacher,
    COUNT(0)                                            AS 'working days',
    COUNT(CASE WHEN too_many_clock_hours THEN 1 END)    AS 'excessively long working days',
    MAX(contact_hours)                                  AS 'maximum clock hours',
    SEC_TO_TIME(AVG(TIME_TO_SEC(contact_hours)))        AS 'average clock hours'
FROM (
    SELECT
        datum,
        teacher,
        SEC_TO_TIME(SUM(TIME_TO_SEC(duration)))              AS contact_hours,
        SEC_TO_TIME(SUM(TIME_TO_SEC(duration))) > '10:00:00' AS too_many_clock_hours
    FROM (
        -- activities per teacher per timeslot
        SELECT		*
        FROM        tt_sax.activities
        WHERE       teacher IS NOT NULL
        GROUP BY    datum, start, end, teacher
        ) a
    GROUP BY datum, teacher
    ) a
GROUP BY teacher