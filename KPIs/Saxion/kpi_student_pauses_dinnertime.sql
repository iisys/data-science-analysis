-- Students: at least 1 free college hour between 16:30 and 18:30 for part time classes.
SET @pauselength      = '00:45:00',
    @bound_low        = '16:30:00',
    @bound_high       = '18:30:00';

-- Approximate runtime: 6 seconds.
SELECT
    class,
    (SELECT     COUNT(DISTINCT datum) 
     FROM       tt_sax.activities ai 
     WHERE      ai.class = a.class)                                 AS `scheduled days`,
    COUNT(0)                                                        AS `scheduled days around dinner time`,
    COUNT(CASE WHEN too_short_pause THEN 1 END)                     AS `days with too short dinner time`
FROM (
    SELECT 
        datum,
        class,
        -- This looks complicated, but it works for all use cases. Try it out!
        GREATEST(
            MAX(LEAST(GREATEST(TIMEDIFF(@bound_high, end), '00:00:00'), COALESCE(next_class_activity, '838:59:59'))), 
            TIMEDIFF(MIN(start), @bound_low),
            TIMEDIFF(@bound_high, MAX(end)),
            '00:00:00'
        )                                                           AS max_free_period_during_dinner,
        GREATEST(
            MAX(LEAST(GREATEST(TIMEDIFF(@bound_high, end), '00:00:00'), COALESCE(next_class_activity, '838:59:59'))),
            TIMEDIFF(MIN(start), @bound_low),
            TIMEDIFF(@bound_high, MAX(end)),
            '00:00:00'
        ) < @pauselength                                            AS too_short_pause
    FROM (
        -- Get only the activities that touch the dinner time period.
        SELECT      a.*
        FROM        tt_sax.activities a
        INNER JOIN  tt_sax.classes c        ON a.class = c.class
        WHERE       c.timetype = 'D'
          AND       (a.start >= @bound_low  OR a.end >  @bound_low)
          AND       (a.start <  @bound_high OR a.end <= @bound_high)
        GROUP BY    a.datum, a.start, a.end, a.class
      ) a
    GROUP BY datum, class
  ) a
GROUP BY class