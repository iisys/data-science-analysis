-- Teacher: max 3 different buildings per day.
-- Teacher: max 2 different cities per day.

-- Approximate runtime: 17.5 minutes.
SELECT
    teacher,
    COUNT(0)                                                                AS `working days`,
    COUNT(CASE WHEN too_many_buildings THEN 1 END)                          AS `days in too many buildings`,
    COUNT(CASE WHEN too_many_cities    THEN 1 END)                          AS `days in too many cities`
FROM (
    SELECT
        teacher,
        datum,
        count(0)                                                                                          AS sequential_activities,
        tt_sax.CORRECT_COUNT(GROUP_CONCAT(DISTINCT cities    ORDER BY cities    SEPARATOR ','), ',')      AS city_count,
        tt_sax.CORRECT_COUNT(GROUP_CONCAT(DISTINCT buildings ORDER BY buildings SEPARATOR ','), ',')      AS building_count,
        COUNT(0) >= 2 AND 
         tt_sax.CORRECT_COUNT(GROUP_CONCAT(DISTINCT cities    ORDER BY cities    SEPARATOR ','), ',') > 2 AS too_many_cities,
        COUNT(0) >= 3 AND 
         tt_sax.CORRECT_COUNT(GROUP_CONCAT(DISTINCT buildings ORDER BY buildings SEPARATOR ','), ',') > 3 AS too_many_buildings
    FROM (
        -- Locations per teacher's activity
        SELECT
            a.teacher,
            a.datum, a.start, a.end,
            count(0) AS activities,
            GROUP_CONCAT(DISTINCT city     ORDER BY city     SEPARATOR ',') AS cities,
            GROUP_CONCAT(DISTINCT building ORDER BY building SEPARATOR ',') AS buildings
        FROM        tt_sax.activities a
        INNER JOIN  tt_sax.activities_locations l   ON l.aid = a.aid
        WHERE       a.teacher IS NOT NULL
        GROUP BY    a.datum, a.start, a.end, a.teacher
        ) a
    GROUP BY teacher, datum
  ) a
GROUP BY teacher