-- Students: minimum of  4 lesson hours per day.
-- Students: maximum of  8 lesson hours per day for fulltime classes.
-- Students: maximum of 10 lesson hours per day for parttime classes.
-- Students: maximum of  2 sequential free hours per day.
-- Students: maximum of  3            free hours per day.

-- Approximate runtime: 75 seconds.
SELECT
    class,
    COUNT(0)                                            AS `scheduled days`,
    COUNT(CASE WHEN too_few_contact_hours   THEN 1 END) AS `days with too few lesson hours`,
    COUNT(CASE WHEN too_many_contact_hours  THEN 1 END) AS `days with too many lesson hours`,
    COUNT(CASE WHEN too_long_free_period    THEN 1 END) AS `days with too many sequential free hours`,
    COUNT(CASE WHEN too_many_free_hours     THEN 1 END) AS `days with too many free hours`,
    timetype
FROM (
    SELECT
        datum,
        class,
        timetype,
        SEC_TO_TIME(SUM(TIME_TO_SEC(duration))) < '03:00:00'                        AS too_few_contact_hours,  --  =  4 lesson hours
        timetype = 'D' AND SEC_TO_TIME(SUM(TIME_TO_SEC(duration))) > '07:30:00' OR                             --  = 10 lesson hours
        timetype = 'V' AND SEC_TO_TIME(SUM(TIME_TO_SEC(duration))) > '06:00:00'     AS too_many_contact_hours, --  =  8 lesson hours
        MAX(next_class_activity) > '01:45:00'                                       AS too_long_free_period,   --  =  2 lesson hours + 15 minutes to account for breaks
        SEC_TO_TIME(SUM(
            CASE WHEN next_class_activity > '00:15:00' THEN TIME_TO_SEC(next_class_activity) END -- don't take into account the 15 min breaks between lesson hours
        )) > '02:15:00'                                                             AS too_many_free_hours     --  =  3 lesson hours
    FROM (
        -- activities per class per timeslot
        SELECT      a.*, 
                    c.timetype
        FROM        tt_sax.activities a
        INNER JOIN  tt_sax.classes c       ON a.class = c.class
        WHERE       a.class IS NOT NULL
        GROUP BY    datum, start, end, class
        ) g
    GROUP BY datum, class
    ) a
GROUP BY class