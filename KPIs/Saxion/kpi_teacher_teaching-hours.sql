-- Teachers: maximum of 8 college hours per day.

SELECT
    datum,
    teacher,
    SEC_TO_TIME(SUM(TIME_TO_SEC(duration)))              AS contact_hours,
    SEC_TO_TIME(SUM(TIME_TO_SEC(next_class_activity)))   AS free_hours,
    MAX(next_class_activity)                AS max_free_period,
    SEC_TO_TIME(SUM(TIME_TO_SEC(duration))) > '06:00:00' as too_many_contact_hours --  =  8 college hours
FROM (
    -- activities per teacher per timeslot
    SELECT		*
    FROM        activities
    WHERE       teacher IS NOT NULL
      AND       type = 'L'
    GROUP BY    datum, start, end, teacher
    ) a
GROUP BY datum, teacher
HAVING   too_many_contact_hours