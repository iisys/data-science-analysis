-- Student: max 2 different buildings per day.

-- Approximate runtime: 3.5 minutes.
SELECT
    class,
    COUNT(0)                                                                                                AS `scheduled days`,
    COUNT(CASE WHEN too_many_buildings THEN 1 END)                                                          AS `days with too many buildings`
FROM (
    SELECT
        class,
        datum,
        COUNT(0) > 2 AND -- only when there are at least three activities, there is a possibility for three different buildings.
        tt_sax.CORRECT_COUNT(GROUP_CONCAT(DISTINCT buildings ORDER BY buildings SEPARATOR ','), ',') > 2    AS too_many_buildings
    FROM (
        -- Building count per class activity.
        SELECT
            a.class,
            a.datum, a.start, a.end,
            GROUP_CONCAT(DISTINCT l.building ORDER BY l.building SEPARATOR ',') AS buildings
        FROM        tt_sax.activities a
        INNER JOIN  tt_sax.activities_locations l   ON l.aid = a.aid
        WHERE       a.class IS NOT NULL
        GROUP BY    a.datum, a.start, a.end, a.class
        ) a
    GROUP BY class, datum
  ) a
GROUP BY class