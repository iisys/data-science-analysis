-- Teachers: if evening was later than 20:30, plan the next morning (until 10:00) off.
SET @next_morning_end = '10:00:00';

-- Approximate runtime: 5.5 minutes.
SELECT
    teacher,
    COUNT(0)                                            AS `scheduled days`,
    COUNT(CASE WHEN late_night            THEN 1 END)   AS `late nights`,
    COUNT(CASE WHEN too_busy_next_morning THEN 1 END)   AS `late nights with planned next morning`
FROM (
    SELECT
        datum,
        teacher,
        MAX(end) > '20:30:00'                           AS late_night,
        IF(MAX(end) > '20:30:00',
            (
                SELECT  COUNT(0)
                FROM    tt_sax.activities a1
                WHERE   a.teacher = a1.teacher
                  AND   a1.datum = DATE_ADD(a.datum, INTERVAL 1 DAY)
                  AND   a1.start < @next_morning_end
            ) > 0, 0)                                   AS too_busy_next_morning
    FROM 
        tt_sax.activities a
    WHERE
        teacher IS NOT NULL
    GROUP BY 
        datum, teacher
  ) a
GROUP BY teacher
