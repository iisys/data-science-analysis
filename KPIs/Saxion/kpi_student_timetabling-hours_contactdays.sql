-- Students: maximum of 4 days per week.
-- dataset changes:
-- -- only L type activities
-- -- no first week of the academic year
-- -- no irrelevant activities
-- -- only activities with a teacher
SET @irrelevant_student_activities = 'Toets|PlusProgramma|project|project|^Stage(activiteiten)?$|^Exam$|^Exam check$|Studie(loopbaan)? ?begeleiding|^SLB|^Workshop$|^Extern$|^Externe activiteit$|^Overleg of Vergadering$|^Honours$|^Voorlichting$|^Activiteit$';

-- Approximate runtime: 2.5 minutes.
SELECT
    class,
    count(0)                                                as 'scheduled weeks',
    count(case when days > 4 then 1 end)                    as 'five day weeks'
FROM (
    SELECT
        a.class,
        d.schoolyear,
        d.quartile,
        d.quartile_week,
        COUNT(DISTINCT a.datum) AS days
    FROM
        tt_sax.activities a
    INNER JOIN 
        tt_sax.dates d         ON a.datum = d.datum
    WHERE
            a.class   IS NOT NULL
        AND a.teacher IS NOT NULL
        AND a.type = 'L'
        AND a.name    NOT REGEXP @irrelevant_student_activities
        AND a.remarks NOT REGEXP @irrelevant_student_activities
        AND NOT (d.quartile = 1 AND d.quartile_week = 1)
    GROUP BY
        a.class, d.schoolyear, d.quartile, d.quartile_week
    ) a
GROUP BY
    class